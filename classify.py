import argparse
import json
import pandas as pd
import tensorflow as tf
import tflearn

#get all the data from a specific order
def get_data_order(data, order_name):
    new_data = data
    for i in range(len(data)):

        if (new_data[i][2] == order_name):
            new_data[i][2] = [1,0]
        else:
            new_data[i][2] = [0,1]

    return new_data

def merge_amino_nuc(amino, nuc):
    final_df = nuc
    return final_df.join(amino.iloc[:, :2].set_index('pid'), on='pid', how='inner')

def read_json_trainer(files_name):
	nuc_data= []
	amino_data = []
	i = 0 #represents the block
	for f in files_name:
		
		with open(f) as json_file:  
			file = json.load(json_file)
	
		nuc_data.append([])
		amino_data.append([])
	
		j=0
		for data in file:
			for pid in data:
				nuc_data[i].append([])
				amino_data[i].append([])

				nuc_data[i][j].append(pid)
				nuc_data[i][j].append(data[pid]['features']['NU'])
				nuc_data[i][j].append(data[pid]['label'])
				nuc_data[i][j].append(data[pid]['type'])

				amino_data[i][j].append(pid)
				amino_data[i][j].append(data[pid]['features']['AA'])
				amino_data[i][j].append(data[pid]['label'])
				amino_data[i][j].append(data[pid]['type'])
		
			j+= 1        
		i += 1 

	feature_names_AA = data[pid]['features_names']['AA'] 
	feature_names_NU = data[pid]['features_names']['NU']

	return nuc_data, amino_data,feature_names_AA,feature_names_NU

def get_train_test_data(nu_list,aa_list):
    index_pid = 0
    index_x = 1
    index_y = 2
    index_type = 3
    train_aa_x = [] 
    train_nu_x = []
    data_y_train = [] 
    test_aa_x = []
    test_nu_x = []
    data_y_test = []
    pids_test = []

    
    for i in range(len(nu_list)):
        if ((nu_list[i][index_type]) == 'train'):
        	train_nu_x.append(nu_list[i][index_x])
        	train_aa_x.append(aa_list[i][index_x])
        	data_y_train.append(nu_list[i][index_y])
        else:
        	
        	test_nu_x.append(nu_list[i][index_x])
        	test_aa_x.append(aa_list[i][index_x])
        	data_y_test.append(nu_list[i][index_y])
        	pids_test.append(nu_list[i][index_pid])
            
            
    return train_aa_x, train_nu_x, data_y_train, test_aa_x, test_nu_x, data_y_test, pids_test


def build_DNN_Arch1(aa_length, nu_length, y_length):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net_aa = tflearn.input_data(shape=[None, aa_length], name="InputData0")
    net_nu = tflearn.input_data(shape=[None, nu_length], name="InputData1")
    # Two fully connected layers with 8 hidden units/neurons
    net_aa = tflearn.fully_connected(net_aa, 4)
    net_nu = tflearn.fully_connected(net_nu, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.merge_outputs([net_aa, net_nu])
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, y_length, activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net, optimizer='adam')

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_arch1_logs', tensorboard_verbose=3)
    return model

def build_DNN_Arch2(aa_length, nu_length, y_length):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net_aa = tflearn.input_data(shape=[None, aa_length], name="InputData0")
    net_nu = tflearn.input_data(shape=[None, nu_length], name="InputData1")
    # Two fully connected layers with 8 hidden units/neurons
#     net_aa = tflearn.fully_connected(net_aa, 4)
    net_nu = tflearn.fully_connected(net_nu, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.merge_outputs([net_aa, net_nu])
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, y_length, activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net, optimizer='adam')

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_arch2_logs', tensorboard_verbose=3)
    return model

def predict(model, test_aa, test_nu,classes):
    results = model.predict({"InputData0":[test_aa],"InputData1":[test_nu]})[0]

    new_results = results
    if results[0] > results[1]:
        #new_results[0] = results[0]
        #new_results[1] = results[1]
        class_result = 0
    else:
        #new_results[0] = results[1]
        #new_results[1] = results[0]
        class_result = 1

    #return_list = []
    #return_list.append((classes[class_result], str(new_results[0])))
    return classes[class_result], float(results[0]), float(results[1])

def make_df_predictions(model,test_aa_x,test_nu_x,test_y, pids,classes,grp):
    class_list =[]
    scores_pos = []
    scores_neg = []
    for i in range(len(test_aa_x)):
        cls, scr_pos,scr_neg = predict(model, test_aa_x[i], test_nu_x[i],classes)
        class_list.append(cls)
        scores_pos.append(scr_pos)
        scores_neg.append(scr_neg)
    score_pos_name = 'score_pos_%s' % (grp)
    score_neg_name = 'score_neg_%s' % (grp)
    predict_class_name = 'predict_class_%s' % (grp)
    test_name = 'test_%s' % (grp)
    df = pd.DataFrame({'pid': pids, score_pos_name: scores_pos, score_neg_name: scores_neg, predict_class_name: class_list, test_name: test_y})
    return df

def main():

	ap = argparse.ArgumentParser()
	ap.add_argument('-m','--models', nargs='+', help="trained models", required=True) # Use like: python train.py -m model1 model2 fmodel3 model4	
	ap.add_argument('-f','--features_file', nargs='+', help="Features Files", required=True) # Use like: python train.py -f features_file1 features_file2 features_file3 features_file4	
	ap.add_argument("-o","--output_name",required=True,help="Output name")
	ap.add_argument("-t","--label_target",required=True,help="Label target")

	args = vars(ap.parse_args())

	files_name = args["features_file"]
	models_name = args["models"]
	output_name = args["output_name"]
	cls_target = args["label_target"]

	classes = [1, 0]

	#Read json file
	nuc_data_all, amino_data_all, _, _ = read_json_trainer(files_name)

	tt_dict = {}
	for i in range(len(files_name)):

		index = str(i)
		nuc_x_name = 'ndata_x%s' % (index)
		amino_x_name = 'adata_x%s' % (index)
		data_y_name = 'data_y%s' % (index)
		tt_dict[i] = {}

		nuc_data = nuc_data_all[i]
		nuc_data = get_data_order(nuc_data, cls_target)
		nuc_data = pd.DataFrame(data = nuc_data, columns=['pid', nuc_x_name, data_y_name,'type'])

		amino_data = amino_data_all[i]
		df_amino = pd.DataFrame(data = amino_data, columns=['pid', amino_x_name, data_y_name,'type'])
		df_amino = df_amino[['pid',data_y_name]]
		#print(df_amino.head(5))
		amino_data = get_data_order(amino_data, cls_target)
		amino_data = [item[:3] for item in amino_data]
		amino_data = pd.DataFrame(data = amino_data, columns=['pid', amino_x_name, data_y_name])
		#print(amino_data)
		#print(nuc_data)

		final_df = merge_amino_nuc(amino_data, nuc_data)
		nu_list = final_df[['pid',nuc_x_name,data_y_name,'type']].values.tolist()
		aa_list = final_df[['pid',amino_x_name,data_y_name,'type']].values.tolist()

		train_aa_x, train_nu_x, data_y_train, test_aa_x, test_nu_x, test_y, test_pid = get_train_test_data(nu_list,aa_list)

		#print(test_aa_x)
		#print(len(test_y[0]))
		model = build_DNN_Arch1(len(test_aa_x[0]), len(test_nu_x[0]), len(test_y[0]))
		model.load(models_name[i]) 
		df = make_df_predictions(model,test_aa_x,test_nu_x, test_y, test_pid,classes,i)

		df_csv = df[['pid','predict_class_%s' %index]].join(df_amino.set_index('pid'), on='pid', how='inner')
		df_csv = df_csv.assign(tf_file = models_name[i])
		df_csv.rename(columns={data_y_name:'label'},inplace=True)
		df_csv.to_csv('output_data/'+output_name+"_"+index+".tsv",index=False,sep='\t')
		


main()