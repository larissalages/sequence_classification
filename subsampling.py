import sys, pickle
import random

def remove(l,n):
    return random.sample(l,int(len(l)*(1-n)))

def save_object(obj, filename):
    with open(filename, 'wb') as output:  
        pickle.dump(obj, output, protocol=2)

if __name__ == '__main__':
	tt_file = sys.argv[1]
	reduce_percent = sys.argv[2]
	reduce_percent = float(reduce_percent)
	train_count = 0
	train_data = []
	test_count = 0
	test_data = []
	with open("train_and_test/" + tt_file, 'rb') as file:
		data = pickle.load(file)
		for i in range(len(data)):
			if data[i][3][0] == 'train':
				train_data.append(data[i])
				train_count += 1
			elif data[i][3][0] == 'test':
				test_data.append(data[i])
				test_count += 1
	train_data = remove(train_data, reduce_percent)
	test_data = remove(test_data, reduce_percent)
	sample_data = train_data + test_data
	
	save_object(sample_data, "sample_data/sample-" + str(reduce_percent) + "-" + tt_file)
