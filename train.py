import nltk

# things we need for Tensorflow
import numpy as np
import tflearn
import tensorflow as tf
import random
import pickle
from collections import Counter
from sklearn.model_selection import KFold, cross_val_score
import argparse


def build_DNN(train_x,train_y):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    # Two fully connected layers with 8 hidden units/neurons - optimal for this task
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.fully_connected(net, len(train_y[0]), activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net)

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_sequence_logs')
    return model

def build_DNN_type_one(aa_length, nu_length, y_length):
	tf.reset_default_graph()
	net_aa = tflearn.input_data(shape=[None, aa_length], name="aa")
	net_nu = tflearn.input_data(shape=[None, nu_length], name="nu")

	net_aa = tflearn.fully_connected(net_aa, 4)
	net_nu = tflearn.fully_connected(net_nu, 4)

	net = tflearn.merge([net_aa, net_nu], 'concat')
	net = tflearn.fully_connected(net, 4)

	net = tflearn.fully_connected(net, y_length, activation='softmax')
	net = tflearn.regression(net)

	model = tflearn.DNN(net, tensorboard_dir='tflearn_sequence_logs')
	return model

def build_DNN_type_two(aa_length, nu_length, y_length):
	tf.reset_default_graph()
	net_aa = tflearn.input_data(shape=[None, aa_length], name="aa")
	net_nu = tflearn.input_data(shape=[None, nu_length], name="nu")

	net_aa = tflearn.fully_connected(net_aa, 4)
	# net_nu = tflearn.fully_connected(net_nu, 4)

	net = tflearn.merge([net_aa, net_nu], 'concat')
	net = tflearn.fully_connected(net, 4)

	net = tflearn.fully_connected(net, y_length, activation='sigmoid')
	net = tflearn.regression(net)

	model = tflearn.DNN(net, tensorboard_dir='tflearn_sequence_logs')
	return model

def train_DNN(model, train_aa, train_nu, train_y, classes):
    # Start training (apply gradient descent algorithm)
    # n_epoch - number of epoch to run
    # Batch size defines number of samples that going to be propagated through the network.
    model.fit([train_aa, train_nu], train_y, n_epoch=5, batch_size=5, show_metric=True)
    model.save('model/'+classes[0]+'_'+classes[1]+'.tflearn')

def main():

	ap = argparse.ArgumentParser()
	ap.add_argument("-i","--input",required=True,help="Input training data")
	ap.add_argument("-c1","--classe1",required=True,help="classe 1")
	ap.add_argument("-c2","--classe2",required=True,help="classe 2")
	ap.add_argument("-t","--type",required=False,help="Type of the DNN")
	
	args = vars(ap.parse_args())

	file_name = args["input"]
	c1 = args["classe1"]
	c2 = args["classe2"]
	dnn_type = args["type"]
	classes = [c1,c2] 

	with open(file_name, 'rb') as file:
		data = pickle.load(file)
    
	data_aa = []
	data_nu = []
	data_y = []
	for i in range(len(data)):
		if ((data[i][3][0]) == 'train'):
			data_aa.append(list(data[i][1][:34]))
			data_nu.append(list(data[i][1][34:]))
			data_y.append(list(data[i][2]))

	# print(data_aa[0])
	# print(data_nu[0])
	# print(data_y)
	#build architecture
	if dnn_type == "1":
		model = build_DNN_type_one(len(data_aa[0]),len(data_nu[0]),len(data_y[0]))
	elif dnn_type == "2":
		model = build_DNN_type_two(len(data_aa[0]),len(data_nu[0]),len(data_y[0]))
	else:
		model = build_DNN(data_x,data_y)
	print(data_aa[0])
	print(data_nu[0])
	print(data_y[0])
	train_DNN(model, data_aa, data_nu, data_y, classes)

main()