import os, sys, json
import numpy as np
from Bio import SeqIO
from Bio.SeqIO.FastaIO import SimpleFastaParser
import pickle
from itertools import product
import random
import multiprocessing




def batch_iterator(iterator, batch_size):
    """Returns lists of length batch_size.

    This can be used on any iterator, for example to batch up
    SeqRecord objects from Bio.SeqIO.parse(...), or to batch
    Alignment objects from Bio.AlignIO.parse(...), or simply
    lines from a file handle.

    This is a generator function, and it returns lists of the
    entries from the supplied iterator.  Each list will have
    batch_size entries, although the final list may be shorter.
    """
    entry = True  # Make sure we loop once
    while entry:
        batch = []
        while len(batch) < batch_size:
            try:
                entry = next(iterator)
            except StopIteration:
                entry = None
            if entry is None:
                # End of file
                break
            batch.append(entry)
        if batch:
            yield batch


def save_object(obj, filename):
    with open(filename, 'wb') as output:  
        pickle.dump(obj, output, protocol=2)

def get_features(ratio_dict):
	features = {}
	for label in ratio_dict:
		for k_mer in ratio_dict[label]:
			if k_mer not in features:
				features[k_mer] = []
	return features


def list_files(directory):
	list_file = os.listdir(directory)
	return list_file


def get_frequency(data):
	frequency = {}
	data_sum = sum(data.values())
	for k_mer in data:
		frequency[k_mer] = float("{0:.10f}".format(float(data[k_mer]) / float(data_sum)))
	return frequency


def get_k_smallest_ratio(ratio_set, top_k):
	smallest_ratio = {}
	for order in ratio_set:
		for k in ratio_set[order].copy():
			if ratio_set[order][k] == 0.0:
				ratio_set[order].pop(k)
		key_list = sorted(ratio_set[order], key=lambda k: ratio_set[order][k], reverse=False)
		value_list = sorted(ratio_set[order].values(), reverse=False)
		smallest_ratio[order] = {}
		for index in range(top_k):
			smallest_ratio[order][key_list[index]] = value_list[index]

	return smallest_ratio


def get_variance(k_mer_list):
	variance = {}
	for k_mer in k_mer_list:
		variance[k_mer] = np.var(k_mer_list[k_mer])
	return variance


def get_ratio(each_set, all_set):
	ratio = {}
	for order in each_set:
		ratio[order] = {}
		for k in each_set[order]:
			if all_set[k] != 0:
				ratio[order][k] = float(each_set[order][k]) / float(all_set[k])
	return ratio

def get_frequency_data(filename, order, keys, count, k_value):
	frequency_data = {}
	with open(filename) as in_handle:
		frequency_data[order] = {}
		for title, seq in SimpleFastaParser(in_handle):
			count_result = {}
			count_result[count] = dict.fromkeys(keys)
			seq = seq.upper()
			for index in range(0, len(seq)-k_value+1):
				k_mer = seq[index:index+k_value]
				if k_mer in count_result[count]:
					if count_result[count][k_mer] is None:
						count_result[count][k_mer] = 1
					else:
						count_result[count][k_mer] += 1
			for key in count_result[count]:
				if count_result[count][key] is None:
					count_result[count][key] = 0
			frequency_data[order][count] = get_frequency(count_result[count])
			count = int(count)
			count += 1
	return frequency_data


def get_key_product(k_value, fastaFileList, directory):
	# fastaFileList = list_files(directory)
	k_value = int(k_value)
	folder_name = str()
	k_mer_list = []
	
	for fastaFile in fastaFileList:
		order = fastaFile.replace('.txt', '').replace('.fasta', '')
		folder_name = order + "_amino"
		file = 'data/'+directory+'/'+fastaFile
		
		record_iter = SeqIO.parse(open(file), "fasta")
		for i, batch in enumerate(batch_iterator(record_iter, 50)):
			filename = "data/%s/group_%i.fasta" % (folder_name, i + 1)
			with open(filename) as in_handle:
				for title, seq in SimpleFastaParser(in_handle):
					seq = seq.upper()
					for index in range(0, len(seq)-k_value+1):
						k_mer = seq[index:index+k_value]
						if ('-' not in k_mer) and ('*' not in k_mer) and ('X' not in k_mer): 
							if k_mer not in k_mer_list:
								k_mer_list.append(k_mer)
	return k_mer_list

def k_mer_ratio_generator(k_value, fastaFileList, keys, directory):
	k_value = int(k_value)
	variance_set = {}
	all_k_mer_list = {}
	folder_name = str()
	for fastaFile in fastaFileList:
		order = fastaFile.replace('.txt', '').replace('.fasta', '')
		
		if directory == "edit_nuc":
			folder_name = order
		elif directory == "edit_amino":
			folder_name = order + "_amino"
		k_mer_list = {}
		file = 'data/' +directory+'/'+fastaFile
		#amino and nucleo diff
		
		record_iter = SeqIO.parse(open(file), "fasta")
		count = "0"
		for i, batch in enumerate(batch_iterator(record_iter, 50)):
			filename = "data/%s/group_%i.fasta" % (folder_name, i + 1)
			# with open(filename, "w") as handle:
			# 	SeqIO.write(batch, handle, "fasta")

			frequency_data = get_frequency_data(filename, order, keys, count, k_value)

			#o stands for order, c stands for label, k stands for k-mer
			for o in frequency_data:
				for c in frequency_data[o]:
					for k in frequency_data[o][c]:
						if k not in k_mer_list:
							k_mer_list[k] = []
						k_mer_list[k].append(frequency_data[o][c][k])
							
		variance_set[order] = get_variance(k_mer_list)
		
		
		#append to the total set of k-mer
		for k in k_mer_list:
			if k not in all_k_mer_list:
				all_k_mer_list[k] = []
			all_k_mer_list[k].extend(k_mer_list[k])
		

		k_mer_list['label'] = order
		
	all_variance_set = get_variance(all_k_mer_list)
	ratio_set = get_ratio(variance_set, all_variance_set)
	smallest_ratio_dict = get_k_smallest_ratio(ratio_set, top_k_ratio)
	return smallest_ratio_dict

def get_k_mer_list(k_value, fastaFileList, keys, directory, features):
	k_value = int(k_value)
	folder_name = str()
	
	for fastaFile in fastaFileList:
		order = fastaFile.replace('.txt', '').replace('.fasta', '')
		if directory == "edit_nuc":
			folder_name = order
		elif directory == "edit_amino":
			folder_name = order + "_amino"		
		file = 'data/' + directory+'/'+fastaFile
		record_iter = SeqIO.parse(open(file), "fasta")
		for i, batch in enumerate(batch_iterator(record_iter, 50)):
			filename = "data/%s/group_%i.fasta" % (folder_name, i + 1)
			with open(filename) as in_handle:
				frequency_data= {}
				for title, seq in SimpleFastaParser(in_handle):
					title = title[:title.index("|")]
					process_id.append(title)
					if title not in features_dict:
						features_dict[title] = dict.fromkeys(features)
					count_result = {}
					count_result[title] = dict.fromkeys(keys)
					seq = seq.upper()
					for index in range(0, len(seq)-k_value+1):
						k_mer = seq[index:index+k_value]
						if k_mer in count_result[title]:
							if count_result[title][k_mer] is None:
								count_result[title][k_mer] = 1
							else:
								count_result[title][k_mer] += 1
					for key in count_result[title]:
						if count_result[title][key] is None:
							count_result[title][key] = 0
					frequency_data[title] = get_frequency(count_result[title])
					#get y_data
					temp_array = [0] * len(fastaFileList)
					temp_array[fastaFileList.index(fastaFile)] = 1
					train_y.append(temp_array)
				for name in frequency_data:
					for key in features:
						if key in frequency_data[name]:
							features_dict[name][key] = frequency_data[name][key]


if __name__ == '__main__':
	sequence_directory = "edit_nuc"
	amino_directory = "edit_amino"
	amino_k_values = sys.argv[1]
	nucleo_k_values = sys.argv[2]
	files = sys.argv[3]
	files = files.split("-")
	top_k_ratio = sys.argv[4]
	top_k_ratio = int(top_k_ratio)
	features_list = {"edit_amino": {}, "edit_nuc": {}}
	final_features = []
	amino_keys = []
	nuc_keys = []

	for i in range(len(amino_k_values)):
		amino_k_value = amino_k_values[i]
		amino_keys = get_key_product(amino_k_value, files, amino_directory)
		ratio = k_mer_ratio_generator(amino_k_value, files, amino_keys, amino_directory)
		features = get_features(ratio)
		features_list["edit_amino"].update(features)

	for i in range(len(nucleo_k_values)):
		nucleo_k_value = nucleo_k_values[i]
		nuc_keys = list(map(''.join, product('ACTG', repeat=int(nucleo_k_value))))
		ratio = k_mer_ratio_generator(nucleo_k_value, files, nuc_keys, sequence_directory)
		features = get_features(ratio)
		features_list["edit_nuc"].update(features)

	for folder in features_list:
		final_features.extend(list(features_list[folder].keys()))
	features_dict = {}
	train_x = []
	train_y = []
	final_feature_data = []
	process_id = []


	for i in range(len(nucleo_k_values)):
		nucleo_k_value = nucleo_k_values[i]
		nuc_keys = list(map(''.join, product('ACTG', repeat=int(nucleo_k_value))))
		get_k_mer_list(nucleo_k_value, files, nuc_keys, sequence_directory, final_features)

	for i in range(len(amino_k_values)):
		amino_k_value = amino_k_values[i]
		get_k_mer_list(amino_k_value, files, amino_keys, amino_directory, final_features)

	for name in features_dict:
		train_x.append(list(features_dict[name].values()))

	for i in range(len(train_x)):
		final_feature_data.append([[process_id[i]], train_x[i], train_y[i]])
	random.shuffle(final_feature_data)
	file_str = ""
	for index, file in enumerate(files):
		file = file.replace(".fasta", "")
		file_str += file
	save_object(final_feature_data, "feature_data/" + amino_k_values + nucleo_k_values + "-mer-top-" + str(top_k_ratio)+ file_str + "-ratio.pkl")
