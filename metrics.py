import pickle
import argparse
import pandas as pd
import re
from numpy import array
from sklearn.metrics import roc_curve, auc
import matplotlib 
matplotlib.use('agg')
import matplotlib.pyplot as plt

def fix_format(string):
    return re.sub('[\[\]\']','',string)

def plot_roc(y_test,y_score):
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    y_test = array(y_test)
    y_score = array(y_score)
    
    fpr, tpr, thresholds = roc_curve(y_test, y_score)
    
    roc_auc = auc(fpr, tpr)

    
    plt.figure()
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',lw=lw, label='ROC curve (area = %0.5f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()

def metrics_DNN(predict_y,id_test,test_y,classes,scores):
    
    true_positive = 0
    true_negative = 0
    false_positive = 0
    false_negative = 0
    id_misclassified = []
    pred_misclassified = []
    y_misclassified = []
    
    #class[0] is the positive class, test_y == 1 is the positive class
    for i in range(len(predict_y)):
        if (predict_y[i] == classes[0] and test_y[i] == 1):
            true_positive += 1
            
        elif (predict_y[i] == classes[1] and test_y[i] == 0):
            true_negative += 1
            
        elif(predict_y[i] == classes[1] and test_y[i] == 1):
            false_negative += 1
            pred_misclassified.append(predict_y[i])
            id_misclassified.append(id_test[i])
            y_misclassified.append(classes[0])
            
        elif(predict_y[i] == classes[0] and test_y[i] == 0):
            false_positive += 1
            pred_misclassified.append(predict_y[i])
            id_misclassified.append(id_test[i])
            y_misclassified.append(classes[1])
    
    
    df_misclassified = pd.DataFrame({'id': id_misclassified,'predicted': pred_misclassified,'real_class': y_misclassified})
            
    precision_classe0 = float(true_positive)/(true_positive + false_positive)
    sensitivity = float(true_positive)/(true_positive + false_negative) #recall for class0
    F1_score_class0 = 2*(float(precision_classe0*sensitivity)/(precision_classe0 + sensitivity))
    
    precision_classe1 = float(true_negative)/(true_negative + false_negative)
    specificity = float(true_negative)/(false_positive + true_negative)#recall for class1
    F1_score_class1 = 2*(float(precision_classe1*specificity)/(precision_classe1 + specificity))
    
    accuracy = float(true_positive + true_negative)/len(predict_y)
    
    print("Class " + classes[0] +":")
    print("Precision: " + str(precision_classe0))
    print("Sensitivity: " + str(sensitivity))
    print("F1 score: " + str(F1_score_class0))
    
    print("")
    
    print("Class " + classes[1] +":")
    print("Precision: " + str(precision_classe1))
    print("Specificity: " + str(specificity))
    print("F1 score: " + str(F1_score_class1))
    
    print("")
    
    print("Accuracy:")
    print(accuracy)

    plot_roc(test_y,scores)
    
    return df_misclassified

def main():
    #params:
    ap = argparse.ArgumentParser()
    ap.add_argument("-i","--input",required=True,help="Input data ")
    ap.add_argument("-c1","--classe1",required=True,help="classe 1")
    ap.add_argument("-c2","--classe2",required=True,help="classe 2")
    ap.add_argument("-p","--predict_file",required=True,help="predict data")
    
    args = vars(ap.parse_args())

    data_file = args["input"]
    c1 = args["classe1"]
    c2 = args["classe2"]
    predictions_file = args["predict_file"]
    classes = [c1,c2] 

    #Open files
    predict_data = pd.read_csv('output_data/'+predictions_file,sep='\t')
    
    with open(data_file, 'rb') as file:
        data = pickle.load(file)

    #get test data
    data_y_test = []
    id_test = []
    for i in range(len(data)):
        if ((data[i][3][0]) == 'test'):
            data_y_test.append(data[i][2])
            id_test.append(data[i][0][0])



    predict_data['order'] = [fix_format(x) for x in predict_data['order']]

    tes_first_neuron = [item[0] for item in data_y_test]
    #function that calculate all metrics
    scores = predict_data['score']
    df_misclassified = metrics_DNN(predict_data['order'],id_test,tes_first_neuron,classes,scores)
    #save file
    df_misclassified.to_csv('output_data/'+'misclassified_data_'+classes[0]+'_'+classes[1]+'.tsv',index=False,sep='\t')

main()