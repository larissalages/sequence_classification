import argparse
import json
from random import shuffle
import pandas as pd


def read_json(files_name):
	nuc_data= []
	amino_data = []
	i = 0 #represents the block
	for f in files_name:
		
		with open(f) as json_file:  
			file = json.load(json_file)
	
		nuc_data.append([])
		amino_data.append([])
	
		j=0
		for pid in file:
			nuc_data[i].append([])
			amino_data[i].append([])

			nuc_data[i][j].append(pid)
			nuc_data[i][j].append(file[pid]['features']['NU'])
			nuc_data[i][j].append(file[pid]['label'])

			amino_data[i][j].append(pid)
			amino_data[i][j].append(file[pid]['features']['AA'])
			amino_data[i][j].append(file[pid]['label'])
		
			j+= 1        
		i += 1 
 
	feature_names_AA = file[pid]['features_names']['AA'] 
	feature_names_NU = file[pid]['features_names']['NU']

	return nuc_data, amino_data,feature_names_AA,feature_names_NU

def determine(lst):
	return (all(x == 0.0 for x in lst))

#check if nuc or aa have data
def check_zeros(data):
	new_data = []
	for i in data:
		if not determine(i[1]):
			if new_data == None:
				new_data = i
			else:
				new_data.append(i)
	return new_data

#get all the data from a specific order
def get_data_order(data, order_name):
	new_data = data
	for i in range(len(data)):

		if (new_data[i][2] == order_name):
			new_data[i][2] = [1,0]
		else:
			new_data[i][2] = [0,1]

	return new_data

def merge_amino_nuc(amino, nuc):
	final_df = nuc
	return final_df.join(amino.iloc[:, :2].set_index('pid'), on='pid', how='inner')

def split_new(data,training_percent):
	training_percent = float(training_percent)
	for i in range(len(data)):
		if i < int(len(data)*training_percent):
			data[i].append(['train'])
		else:
			data[i].append(['test'])
	return data



def main():

	ap = argparse.ArgumentParser()
	ap.add_argument('-f','--features_file', nargs='+', help="Features Files", required=True) # Use like: python train.py -f features_file1 features_file2 features_file3 features_file4	
	ap.add_argument("-o","--output_name",required=True,help="Output name")

	args = vars(ap.parse_args())

	files_name = args["features_file"]
	output_name = args["output_name"]


	#Read json file
	nuc_data_all, amino_data_all,feature_names_AA,feature_names_NU = read_json(files_name)

	for i in range(len(files_name)):
		index = str(i)

		nuc_x_name = 'ndata_x%s' % (index)
		amino_x_name = 'adata_x%s' % (index)
		data_y_name = 'data_y%s' % (index)

		nuc_data = nuc_data_all[i]
		shuffle(nuc_data)
		nuc_data = check_zeros(nuc_data)
		nuc_data = pd.DataFrame(data = nuc_data, columns=['pid', nuc_x_name, data_y_name])

		amino_data = amino_data_all[i]
		shuffle(amino_data)
		amino_data = check_zeros(amino_data)
		amino_data = pd.DataFrame(data = amino_data, columns=['pid', amino_x_name, data_y_name])
		#remove the NU or AA that does not have information and join by pid
		final_df = merge_amino_nuc(amino_data, nuc_data)
		nu_list = final_df[['pid',nuc_x_name,data_y_name]].values.tolist()
		aa_list = final_df[['pid',amino_x_name,data_y_name]].values.tolist()
		nu_list = split_new(nu_list,0.1)
		aa_list = split_new(aa_list,0.1)

		l_data = []
		for k in range(len(nu_list)):
			dic_ = {}
			pid = nu_list[k][0]
			dic_[pid] = {}
			dic_[pid]["features"] = {}
			dic_[pid]["features"]["AA"] = aa_list[k][1]
			dic_[pid]["features"]["NU"] = nu_list[k][1]
			dic_[pid]["features_names"] = {}
			dic_[pid]["features_names"]["AA"] = feature_names_AA
			dic_[pid]["features_names"]["NU"] = feature_names_NU
			dic_[pid]["label"] = nu_list[k][2]
			dic_[pid]["type"] = nu_list[k][3][0]
			l_data.append(dic_)

		with open(output_name, 'w') as outfile:
			json.dump(l_data, outfile)
			outfile.close()
		

main()