import nltk

# things we need for Tensorflow
import numpy as np
import tflearn
import tensorflow as tf
import pickle
import csv
import pandas as pd
import argparse

def build_DNN(train_x,train_y): 
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net = tflearn.input_data(shape=[None, len(train_x[0])])
    # Two fully connected layers with 8 hidden units/neurons - optimal for this task
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.fully_connected(net, len(train_y[0]), activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net)

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_sequence_logs')
    return model

def build_DNN_type_one(aa_length, nu_length, y_length):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net_aa = tflearn.input_data(shape=[None, aa_length], name="InputData0")
    net_nu = tflearn.input_data(shape=[None, nu_length], name="InputData1")
    # Two fully connected layers with 8 hidden units/neurons
    net_aa = tflearn.fully_connected(net_aa, 4)
    net_nu = tflearn.fully_connected(net_nu, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.merge_outputs([net_aa, net_nu])
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, y_length, activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net, optimizer='adam')

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_arch1_logs', tensorboard_verbose=3)
    return model

def build_DNN_type_two(aa_length, nu_length, y_length):
    tf.reset_default_graph()
    # Build neural network - input data shape, number of words in vocabulary (size of first array element). 
    net_aa = tflearn.input_data(shape=[None, aa_length], name="InputData0")
    net_nu = tflearn.input_data(shape=[None, nu_length], name="InputData1")
    # Two fully connected layers with 8 hidden units/neurons
    #net_aa = tflearn.fully_connected(net_aa, 4)
    net_nu = tflearn.fully_connected(net_nu, 4)
    # number of intents, columns in the matrix train_y
    net = tflearn.merge_outputs([net_aa, net_nu])
    net = tflearn.fully_connected(net, 4)
    net = tflearn.fully_connected(net, y_length, activation='sigmoid')
    # regression to find best parameters, during training
    net = tflearn.regression(net, optimizer='adam')

    # Define Deep Neural Network model and setup tensorboard
    model = tflearn.DNN(net, tensorboard_dir='tflearn_arch1_logs', tensorboard_verbose=3)
    return model

def predict(model, test_aa, test_nu,classes):
    
    results = model.predict({"InputData0":[test_aa],"InputData1":[test_nu]})[0]

    new_results = results
    if results[0] > results[1]:
        class_result = 0
    else:
        class_result = 1

    return classes[class_result], results[0]

def main():

    ap = argparse.ArgumentParser()
    ap.add_argument("-i","--input",required=True,help="Input data")
    ap.add_argument("-c1","--classe1",required=True,help="classe 1")
    ap.add_argument("-c2","--classe2",required=True,help="classe 2")
    ap.add_argument("-m","--model",required=True,help="model generated in training step")
    ap.add_argument("-t","--type",required=False,help="Type of the DNN")
    
    args = vars(ap.parse_args())

    file_name = args["input"]
    c1 = args["classe1"]
    c2 = args["classe2"]
    dnn_type = args["type"]
    classes = [c1,c2]
    model_name = args["model"]

    with open(file_name, 'rb') as file:
        data = pickle.load(file)
    data_aa = []
    data_nu = []
    data_y = []
    pid = []
    print(data[0])
    for i in range(len(data)):
        if ((data[i][3][0]) == 'test'):
            data_aa.append(list(data[i][1][:34]))
            data_nu.append(list(data[i][1][34:]))
            data_y.append(list(data[i][2]))
            pid.append(data[i][0][0])
            
    #build architecture 
    if dnn_type == "1":
        model = build_DNN_type_one(len(data_aa[0]),len(data_nu[0]),len(data_y[0]))
    elif dnn_type == "2":
        model = build_DNN_type_two(len(data_aa[0]),len(data_nu[0]),len(data_y[0]))
    else:
        model = build_DNN(data_x,data_y)
    model.load('model/'+model_name)

    result = []
    scores = []
    for i in range(len(data_aa)):
        cls, score = predict(model, data_aa[i], data_nu[i],classes)
        result.append(cls)
        scores.append(float(score))


    df = pd.DataFrame(pid)
    df.columns = ['id']
    df = df.assign(order=result)
    df = df.assign(score=scores)
    df.to_csv('output_data/predictions_'+c1+'_'+c2+'.tsv', sep='\t',index=False)
    


main()