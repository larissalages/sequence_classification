import sys, pickle

def save_object(obj, filename):
    with open(filename, 'wb') as output:  
        pickle.dump(obj, output, protocol=2)

if __name__ == '__main__':
	feature_file = sys.argv[1]
	training_percent = sys.argv[2]
	training_percent = float(training_percent)
	
	with open("feature_data/" + feature_file, 'rb') as file:
		data = pickle.load(file)
		for i in range(len(data)):
			if i < int(len(data)*training_percent):
				data[i].append(['train'])
			else:
				data[i].append(['test'])
		save_object(data, "train_and_test/tt-" + str(training_percent) + "-" + feature_file)

